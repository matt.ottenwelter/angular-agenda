import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit{

  @Input() moduleName: string | undefined;
  @Input() toto:string | undefined;

  private bgPhp: string = '#9C38F9FF'
  private bgJs: string = '#f9d238'
  private bgHTML: string = '#38f965'
  private bgCss: string = '#f938df'
  private bgTypescript: string = '#3848f9'
  private bgAngular: string = '#f93838'
  private bgSymfony: string = '#38a9f9'
  private borderColor: string = 'solid 3px #F95738'
  private scaleHover: number = 1.3

  constructor(private el: ElementRef) {

  }

  ngOnInit() {

    switch (this.moduleName) {
      case 'PHP':
        this.setColor(this.bgPhp)
        break
      case 'JS':
        this.setColor(this.bgJs)
        break
      case 'HTML':
        this.setColor(this.bgHTML)
        break
      case 'CSS':
        this.setColor(this.bgCss)
        break
      case 'Angular':
        this.setColor(this.bgAngular)
        break
      case 'Symfony':
        this.setColor(this.bgSymfony)
        break
      case 'Typescript':
        this.setColor(this.bgTypescript)
        break
      default:
        break
    }
  }

  setColor(color:string){
    this.el.nativeElement.style.backgroundColor = color
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.el.nativeElement.style.border = this.borderColor;
    this.el.nativeElement.style.transform = 'scale('+this.scaleHover+')'
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.el.nativeElement.style.border = '';
    this.el.nativeElement.style.transform = 'scale(1)'
  }



}
