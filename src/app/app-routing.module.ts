import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DetailModuleComponent} from "./detail-module/detail-module.component";
import {CalendarComponent} from "./calendar/calendar.component";

const routes: Routes = [
  { path: '', component: CalendarComponent},
  {path: 'module/detail/:id', component: DetailModuleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
