import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarComponent } from './calendar/calendar.component';
import { HighlightDirective } from './highlight.directive';
import { DetailModuleComponent } from './detail-module/detail-module.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    HighlightDirective,
    DetailModuleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
