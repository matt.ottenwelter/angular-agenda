import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {LISTMODULE} from "../calendar/mock-module-list";

@Component({
  selector: 'app-detail-module',
  templateUrl: './detail-module.component.html',
  styleUrls: ['./detail-module.component.scss']
})
export class DetailModuleComponent {
  module: { horaire: string; formateur: string; coefficient: number; id: number; nom: string } | undefined;
  moduleList = LISTMODULE

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {
    const moduleID: string | null = this.route.snapshot.paramMap.get('id');
    if (moduleID) {
      this.module = this.moduleList.find(module => module.id == +moduleID)
    }
  }

  gotoMenu() {
    this.router.navigate(['']);
  }

}
