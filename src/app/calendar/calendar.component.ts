import {Component, OnInit} from '@angular/core';
import {LISTMODULE} from "./mock-module-list";
import {Router} from "@angular/router";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})

export class CalendarComponent implements OnInit{
  daysList = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi']
  moduleList = LISTMODULE
  selectedList = this.moduleList
  module: { horaire: string; formateur: string; coefficient: number; id: number; nom: string } | undefined;

  constructor(  private router: Router) {
  }

ngOnInit(){
}

selectModule(id:number){
  this.module = this.moduleList.find(module => module.id == +id)

  if (this.module) {
    this.router.navigate(['/module/detail', this.module.id])
  }

}

search(module: string){
   this.selectedList =  this.moduleList.filter(function (el)
   {
       return el.nom.toLowerCase().includes(module.toLowerCase())
   }
   );
}


}
