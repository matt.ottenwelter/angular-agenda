
export const LISTMODULE  = [
  {
    id : 1,
    day: 'Lundi',
    nom: 'HTML',
    horaire: '2023-01-13T08:30:00',
    coefficient: 1,
    formateur: 'Mr Donatello'
  },
  {
    id : 2,
    day: 'Lundi',
    nom: 'CSS',
    horaire: '2023-01-13T10:30:00',
    coefficient: 1,
    formateur: 'Mme Carmenita'
  },
  {
    id : 3,
    day: 'Lundi',
    nom: 'PHP',
    horaire: '2023-01-13T14:00:00',
    coefficient: 5,
    formateur: 'Mr Mouk'
  },
  {
    id : 4,
    day: 'Lundi',
    nom: 'JS',
    horaire: '2023-01-13T16:00:00',
    coefficient: 3,
    formateur: 'Mr Mouk'
  },
  {
    id : 5,
    day: 'Mardi',
    nom: 'Typescript',
    horaire: '2023-01-14T08:30:00',
    coefficient: 2,
    formateur: 'Mr Ateez'
  },
  {
    id : 6,
    day: 'Mardi',
    nom: 'Angular',
    horaire: '2023-01-14T10:00:00',
    coefficient: 5,
    formateur: 'Mr ftetz'
  },
  {
    id : 7,
    day: 'Mardi',
    nom: 'JS',
    horaire: '2023-01-14T14:00:00',
    coefficient: 3,
    formateur: 'Mr Mouk'
  },
  {
    id : 8,
    day: 'Mardi',
    nom: 'Angular',
    horaire: '2023-01-14T16:00:00',
    coefficient: 6,
    formateur: 'Mr éefé'
  },
  {
    id : 9,
    nom: 'JS',
    day: 'Mercredi',
    horaire: '2023-01-15T08:00:00',
    coefficient: 1,
    formateur: 'Mr scroum'
  },
  {
    id : 10,
    day: 'Mercredi',
    nom: 'Symfony',
    horaire: '2023-01-15T14:00:00',
    coefficient: 5,
    formateur: 'Mr Failcho'
  },
  {
    id : 11,
    day: 'Mercredi',
    nom: 'Vol de voiture',
    horaire: '2023-01-15T16:00:00',
    coefficient: 3,
    formateur: 'Mr Assin'
  },
  {
    id : 12,
    day: 'Mercredi',
    nom: 'CSS',
    horaire: '2023-01-15T18:00:00',
    coefficient: 5,
    formateur: 'Mr Pouloulou'
  },
  {
    id : 13,
    day: 'Jeudi',
    nom: 'Symfony',
    horaire: '2023-01-16T10:00:00',
    coefficient: 2,
    formateur: 'Mr MDJZ'
  },
  {
    id : 14,
    day: 'Jeudi',
    nom: 'HTML',
    horaire: '2023-01-16T10:00:00',
    coefficient: 2,
    formateur: 'Mr AOKN'
  },
  {
    id : 15,
    day: 'Jeudi',
    nom: 'PHP',
    horaire: '2023-01-16T14:00:00',
    coefficient: 1,
    formateur: 'Mr UAYH'
  },
  {
    id : 16,
    day: 'Jeudi',
    nom: 'Typescript',
    horaire: '2023-01-16T16:00:00',
    coefficient: 5,
    formateur: 'Mr POAI'
  },
  {
    id : 17,
    day: 'Vendredi',
    nom: 'FTP',
    horaire: '2023-01-17T08:00:00',
    coefficient: 3,
    formateur: 'Mr AGUS'
  },
  {
    id : 18,
    day: 'Vendredi',
    nom: 'JS',
    horaire: '2023-01-17T10:00:00',
    coefficient: 2,
    formateur: 'Mr azea'
  },
  {
    id : 19,
    day: 'Vendredi',
    nom: 'PHP',
    horaire: '2023-01-17T14:00:00',
    coefficient: 1,
    formateur: 'Mr Mouk'
  },
  {
    id : 20,
    day: 'Vendredi',
    nom: 'Bar',
    horaire: '2023-01-17T16:00:00',
    coefficient: 12,
    formateur: 'Mr Boidlo'
  },
];
